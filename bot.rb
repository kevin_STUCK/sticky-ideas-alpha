require 'telegram_bot'
token = '1527934293:AAHHXxn6C2PiJ2i50TfhHTXxPkna9bWyw5k'
bot = TelegramBot.new(token: token)

bot.get_updates(fail_silently: true) do |message|
    puts "@#{message.from.username}: #{message.text}"
    command = message.get_command_for(bot)
  
    message.reply do |reply|
      case command
      when /start/i
        reply.text = "Hi #{message.from.first_name}, are you stuck?

What would you like help with

Finding new opportunities? 
Try the /find command

Creating solutions? 
Try the /create command"
      when /create/i
        prompts = ['Let’s try 3 ideas where A FRIEND OR BUDDY was the solution?', 'How about we try 4 ideas where we INVOLVE FOOD in the solution', 'What if we try 3 ideas where USERS THEMSELVES became the solution', 'Hmm.. How about we go for 5 ideas that could get you fired', 'How about 2 ideas that MADE PEOPLE COLLABORATE', 'Can we do 3 ideas where we make people FEEL LIKE THEY ARE LEARNING SOMETHING', 'Let’s come up with 2 ideas that involve COMPETITION', 'What if we came up with 4 ideas that involved STATUS, LOSING FACE or SAVING FACE', 'Let’s go for 5 ideas that involve GAIN of privillege', 'Let’s come up with 3 ideas that involve REMOVING something', 'How about we try 2 ideas where the solution was SELF SERVICE']
        reply.text = "okay #{message.from.first_name}, 
#{prompts.sample.capitalize}"
      when /find/i
        reply.text= "How would you like to find new opportunities?

a question? 
Try the /qn command

a way to ask better questions? 
Try the /ask command

or a quick bite to boose your creativty? 
Try the /inspireme command"
      when /inspireme/i
        inspiration = ['https://www.youtube.com/playlist?list=PLl1HOXB4dbFcooWNhZtfJ3hPDTdm5X7p7', 'https://www.fastcompany.com/90356441/the-30-second-trick-that-can-make-anyone-more-creative?utm_source=facebook.com&utm_medium=social&fbclid=IwAR2zN5iu5xl-NoePwi9Hx-7EsSzqgHpzS--RyGcDJvUTWr7KYb6pjXx26bU','http://batux.design/?_lrsc=736364df-01ed-42cd-84d6-eda4a6ca3c7a']
        reply.text = "#{inspiration.sample}"
      when /qn/i
        question = ['who is your specific user?', 'what is the key goal you want to achieve','Can we ask why to find a deeper root to the problem?','What if we could half solve our problem?']
        reply.text = "Try asking your team - #{question.sample}"
      when /ask/i
        ask = ['What are 3 things that work?', 'What are the 3 things that don t work', 'What was your best experience?', 'What was your worst experience','If you had a magic wand what would your version of this experience be?']
        reply.text = "Try asking your user - #{ask.sample}"
      else
        reply.text = "I have no idea what #{command.inspect} means."
      end
      puts "sending #{reply.text.inspect} to @#{message.from.username}"
      reply.send_with(bot)
    end
  end